package net.thumbtack;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RestResource;

/**
 * Account REST resource.
 */
@RestResource
public interface AccountResource extends PagingAndSortingRepository<Account,Long> {
}
