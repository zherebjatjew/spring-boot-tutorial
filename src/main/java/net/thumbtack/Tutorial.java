package net.thumbtack;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.hateoas.UriTemplate;
import org.springframework.hateoas.hal.CurieProvider;
import org.springframework.hateoas.hal.DefaultCurieProvider;

/**
 * Main application class.
 */
@SpringBootApplication
public class Tutorial {
	@Bean
	public CurieProvider curieProvider() {
		return new DefaultCurieProvider("bank", new UriTemplate("http://localhost:8080/alps/{rel}"));
	}

	public static void main(final String[] args) throws Exception {
		SpringApplication.run(Tutorial.class, args);
	}
}
